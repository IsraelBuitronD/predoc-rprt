## ¿Cuál es la sugerencia?

[Descripción detallada de lo que se sugiere]

## ¿Dónde se ubica en el texto?

- Página 2, Párrafo 2, Enunciado 1
- Página 2, Figura 2

## ¿Quién lo sugiere?

[Refiera los nombre de usuario si tienen cuenta en el repositorio, de lo contrario sólo su nombre]

## ¿Cuándo lo sugiere?

[dd-mm-yyyy hh:mm]

/assign @ 
/label ~sugerencia 
/milestone % 


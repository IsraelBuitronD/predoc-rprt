% !TEX root = ../predoc-plain.tex
%
\chapter{Gr\'afica de trayectorias y problemas de la madeja} % (fold)
\label{cha:grafica_de_trayectorias_y_problemas_de_la_madeja}

En este cap\'itulo presentamos una de las vertientes de este trabajo: las gr\'aficas de trayectorias y los problemas de la madeja aplicados en estas gr\'aficas.
Mediante un problema de construcci\'on de trayectorias en una gr\'afica simple no dirigida construimos las gr\'aficas de trayectorias, donde un v\'ertice en esta segunda gr\'afica es una trayectoria en la primera y una pareja de estas trayectorias forman una arista si estas se cruzan o no son ajenas.

Relacionamos el problema de la madeja con el problema de b\'usqueda de conjuntos independientes maximales en teor\'ia de gr\'aficas.
Planteamos que es un problema dif\'icil y con aplicaciones en protocolos de autenticaci\'on.

Probamos que las gr\'aficas de trayectoria no fueran gr\'aficas perfectas o de Berge, ya que se sabe que existen algoritmos de tiempo polinomial para resolver el problema de independencia en este tipo de gr\'aficas.

\section{\texorpdfstring%
  {Gr\'afica de trayectorias $P_{m,k,K,G}$}%
  {Gr\'afica de trayectorias}} % (fold)
\label{sec:grafica_de_trayectorias}

\begin{problem}
  \label{prob:non_crossing_paths}
  Dado un conjunto $K$ de parejas de v\'ertices en una gr\'afica $G$, el \emph{problema de b\'usqueda de trayectorias sin cruces} consiste en encontrar un conjunto maximal de trayectorias sin cruces en $G$ cuyos extremos pertenezcan a $K$.
  \begin{description}
    \item \textbf{Instancia}:
    Una gr\'afica $G = (V,E)$, dos enteros $k,m \in \mathbb{N}$ tales que $k \cdot m \leq |V(G)|$, un conjunto de parejas de v\'ertices $K = {\{(v_{i,\iota},v_{j,\iota}) \; : \; d_G(v_{i,\iota},v_{j,\iota}) \leq m \}}_{\iota = 1}^{k}$.
    \item \textbf{Soluci\'on}:
    Un conjunto maximal $\Pi$ de $m$-trayectorias ajenas y sin cruces a pares
    \[
      \Pi = {\left\{\pi_\iota = [v_{i,\iota},\dots,v_{j,\iota}] \; : \; |\pi_\iota| = m \right\}}_{\iota = 1}^{k}.
    \]
  \end{description}
\end{problem}

\begin{figure}[H]
  \begin{subfigure}[t]{.35\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-noncross_srch_path-base_graph.tikz}
    \end{tikzpicture}
    \caption{Una gr\'afica $G$ para proponer una instancia del problema}
    \label{fig:ejemplo-problema_busqueda_trayectorias-grafica}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{.25\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-noncross_srch_path-instance.tikz}
    \end{tikzpicture}
    \caption{Un conjunto $K$ de parejas de v\'ertices en $G$ como instancia del problema}
    \label{fig:ejemplo-problema_busqueda_trayectorias-instancia}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{.35\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-noncross_srch_path-solution.tikz}
    \end{tikzpicture}
    \caption{Un conjunto $\Pi$ de trayectorias como soluci\'on a la instancia}
    \label{fig:ejemplo-problema_busqueda_trayectorias-solucion}
  \end{subfigure}%
  \caption{Ejemplo de una instancia del problema de b\'usqueda de trayectorias y una soluci\'on de esta}
  \label{fig:ejemplo_problema_busqueda_trayectorias}
\end{figure}

\begin{problem}
  \label{prob:hamiltonian_extension}
  El \emph{problema de extensi\'on hamiltoniana} consiste en encontrar un ciclo hamiltoniano $H$ en una gr\'afica $G$ dada, tal que todos los elementos de un conjunto dado $\Pi$ de trayectorias ajenas y sin cruces a pares sean segmentos propios del ciclo $H$.
  \begin{description}
    \item \textbf{Instancia}:
    Una gr\'afica $G = (V,E)$, un conjunto $\Pi$ de $m$-trayectorias ajenas y sin cruces a pares.
    \item \textbf{Soluci\'on}:
    Un ciclo hamiltoniano $h \in \mathcal{H}_{G}$ tal que para cualquier $\pi = [v_i,\dots,v_j] \in \Pi$ cumple que el segmento propio de $h$ entre $v_i$ y $v_j$, y que denotamos por $\pi_{h}{(v_i,v_j)}$, es el mismo que $\pi$.
  \end{description}
\end{problem}

\begin{figure}[H]
  \begin{subfigure}[t]{.35\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-hamilt_exten_srch-base_graph.tikz}
    \end{tikzpicture}
    \caption{Una gr\'afica $G$ para proponer una instancia del problema}
    \label{fig:ejemplo-problema_extension_hamiltoniana-grafica}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{.25\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-hamilt_exten_srch-instance.tikz}
    \end{tikzpicture}
    \caption{Un conjunto $\Pi$ de trayectorias en $G$ como instancia del problema}
    \label{fig:ejemplo-problema_extension_hamiltoniana-instancia}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{.35\linewidth}
    \centering
    \begin{tikzpicture}
      \input{gfx/tikz/exmp-hamilt_exten_srch-solution.tikz}
    \end{tikzpicture}
    \caption{Un ciclo hamiltoniano $h$ en $G$ como soluci\'on a la instancia}
    \label{fig:ejemplo-problema_extension_hamiltoniana-solucion}
  \end{subfigure}%
  \caption{Ejemplo de una instancia del problema de extensi\'on hamiltoniana y una soluci\'on de esta}
  \label{fig:ejemplo_problema_extension_hamiltoniana}
\end{figure}

N\'otese que para el hipercubo $Q_n$ y un emparejamiento perfecto $\Pi$, el \autoref{prob:hamiltonian_extension} siempre tiene una soluci\'on aunque no es \'unica.
As\'i la extensi\'on hamiltoniana permite reconstruir un ciclo hamiltoniano a partir de una soluci\'on del problema de b\'usqueda de trayectorias sin cruces.
Sin embargo, se puede obtener una soluci\'on de este \'ultimo problema sin construir un ciclo hamiltoniano completo que contenga esa soluci\'on.

Pero resolver el problema de b\'usqueda de trayectorias sin cruces puede reducirse a la b\'usqueda del conjunto independiente maximal en una gr\'afica grande, como veremos.

\begin{definition}
  Dada una instancia del ~\autoref{prob:non_crossing_paths}:
  \[
    \left(
    G,
    k,
    K = \left\{ {(v_{i,\iota},v_{j,\iota})}_{\iota = 1}^{k} \right\},
    m
    \right),
  \]
  La \emph{gr\'afica de $m$-trayectorias}, $P_{m,k,K,G}$, es aquella que tiene como v\'ertices $\pi$ a $m$-trayectorias en $G$ que conectan a las parejas en $K$:
  \[
    \pi = [v_{j 0},\dots,v_{j m}] \in V({P_{m,k,K,G}})
    \iff
    {v_{j 0},v_{j m}} \in K,
  \]
  y sus aristas son de dos tipos:
  \begin{itemize}
    \item si $\pi_1,\pi_2$ tienen cruces, entonces $\pi_1,\pi_2 \in E({P_{m,k,K,G}})$, y
    \item si $\pi_1,\pi_2$ tienen los mismos v\'ertices extremos, entonces $\pi_1,\pi_2 \in E({P_{m,k,K,G}})$.
  \end{itemize}
\end{definition}

%
% section grafica_de_trayectorias (end)
%

\section{\texorpdfstring%
  {Problema de la madeja en $Q_n$}%
  {Problema de la madeja en el hipercubo}} % (fold)
\label{sec:problema_de_la_madeja_en_el_hipercubo}

Un conjunto independiente $I$ de $P_{m,k,K,G}$ es un conjunto de trayectorias no cruzadas y ajenas entre s\'i, con extremos en $K$, y sin alguna pareja de extremos conectada por dos trayectorias.

Para cualquier par $(v_i,v_j) \in K$, sea $R(v_i,v_j)$ la subgr\'afica inducida de $P_{m,k,K,G}$ por el conjunto de v\'ertices en $V({P_{m,k,K,G}})$ que tienen como v\'ertices extremos a $v_i$ y $v_j$.
Entonces, $R(v_i,v_j)$ es una madeja.

Esas madejas producen una partici\'on de los v\'ertices en $P_{m,k,K,G}$ con $k$ subconjuntos y cualquier soluci\'on del \autoref{prob:non_crossing_paths} debe tener exactamente un miembro en cada madeja.
Por lo tanto, tiene a lo m\'as $k$ trayectorias.
As\'i, siempre que exista un conjunto independiente, digamos $I$, que su orden alcance la cota superior $k$, entonces ser\'a un conjunto independiente m\'aximo.
% \replaced%
% {As\'i, siempre que exista un conjunto independiente $I^{\ast}$ que alcance el l\'imite superior $k$, tal que $I^{\ast}$ es m\'aximo.}
% {As\'i, siempre que exista un conjunto independiente, digamos $I$, que su orden alcance la cota m\'axima $k$, entonces es un conjunto independiente m\'aximo.}

\begin{proposition}
  Con la notaci\'on anterior, el par\'ametro k es igual al n\'umero de independencia de $P_{m,k,K,G}$,
  \[
    k = \alpha{(P_{m,k,K,G})},
  \]
   y un conjunto independiente de $P_{m,k,K,G}$ es m\'aximo, si y s\'olo si, es una soluci\'on de una instancia del \autoref{prob:non_crossing_paths} de la forma:
  \[
    \left(
    G,
    k,
    K = \left\{ {(v_{i,\iota},v_{j,\iota})}_{\iota = 1}^{k} \right\},
    m
    \right)
  \]
\end{proposition}

Examinemos algunos criterios para seleccionar instancias del \autoref{prob:non_crossing_paths} para complicar su soluci\'on.

El inter\'es principal en el problema planteado es encontrar conjuntos independientes maximos en la gr\'afica $P_{m,k,K,G}$ para una instancia $\left(
  G,
  k,
  K = \left\{ {(v_{i,\iota},v_{j,\iota})}_{\iota = 1}^{k} \right\},
  m
  \right)$ del \autoref{prob:non_crossing_paths}.

Como hemos acordado antes, consideremos en particular el hipercubo $G = Q_n$ para un entero positivo $n$.
Las aristas en el hipercubo $Q_n$ son parejas de v\'ertices de la forma $(u,v)$ donde $u +v = e_i$ es un vector de la base can\'onica de $Q_n$: todas sus entradas son cero excepto para la $i$-\'esima entrada.
Un ciclo hamiltoniano en $Q_n$ es una sucesi\'on $H = h_0, \dots, h_{2^n - 1}$ tal que sus t\'erminos forman una permutaci\'on de $V(Q_n)$ y cada par continguo de t\'erminos $h_{\kappa}h_{\kappa+1}$ es una arista.

\begin{definition}
  Un \emph{cuadro} o \emph{$4$-ciclo} en $Q_n$ es una sucesi\'on $v_0v_1v_2v_3$ de v\'ertices diferentes a pares que forman un ciclo en el hipercubo.
\end{definition}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[scale=0.6]
    \input{gfx/tikz/4cycle_exmp.tikz}
  \end{tikzpicture}
  \caption{Representaci\'on gr\'afica de un $4$-ciclo}
  \label{fig:4cycle_exmp}
\end{figure}

\begin{remark}
  Forzosamente, todo cuadro tiene la forma:
  \[
    v, v+e_i, v+e_i+e_j, v+e_j
  \]
  para cualesquiera dos \'indices distintos $i,j \in \{0,\dots,n-1\}$ (ver \autoref{fig:4cycle_exmp}).
\end{remark}

El t\'ipico ciclo hamiltoniano en el hipercubo es el C\'odigo de Grey.
Como una sucesi\'on, este c\'odigo est\'a determinado por la recurrencia:

\[
  g_n =
  \begin{cases}
    [0,1] & n = 1
    \\
    \mbox{join}{(0 \ast g_{n-1}, 1 \ast \mbox{rev}(g_{n-1}) )} & n > 1
  \end{cases}
\]
donde $\ast$ es la aplicaci\'on de prefijos a una lista, \texttt{join} y \texttt{rev} son las operaciones de concatenaci\'on de listas y reverso de listas, respectivamente.

\begin{example}
  Las primeras instancias de $g_n$ son:
  \begin{align}
    g_1 & = [0,1]
    \\
    g_2 & = [00,01,11,10]
    \\
    g_3 & = [000,001,011,010,110,111,101,100]
  \end{align}
\end{example}

Para tener una idea sobre la cardinalidad de $V({P_{m,k,K,Q_n}})$, comenzamos por estimar el n\'umero de $m$-trayectorias que conectan dos v\'ertices diferentes en ese hipercubo.

\begin{remark}
  Sea $u_0, u_m \in V(Q_n)$ con $u_0 \neq u_m$.
  El n\'umero de trayectorias que conectan $u_0$ y $u_m$ depende exclusivamente de la distancia de Hamming entre $u_0$ y $u_m$.
\end{remark}

De hecho, si $u_0, u_m$ son otros v\'ertices en $Q_n$ con
\[
  \mbox{Hamming}(u_0,u_m) = \mbox{Hamming}(v_0,v_m) = h
\]
entonces, $u_m = u_0 + \sum_{i = 1}^{h}{e_{k_i}}$ y $v_m = v_0 + \sum_{i = 1}^{h}{e_{k'_i}}$, donde los conjuntos de \'indices $\{k_1, \dots, k_h\}$ y  $\{k'_1, \dots, k'_h\}$ son $h$-subconjuntos en $\{1,\dots,n\}$, es decir, conjuntos con exactamente $h$ elementos.
Para cualquier permutaci\'on $\pi$ de $\{1,\dots,n\}$ tal que
\[
  \left\{ \pi{(k_1)},\dots,\pi{(k_h)} \right\}
  =
  \left\{ k'_1,\dots,k'_h \right\}
\]
tenemos que cualquier $m$-trayectoria $u_0 \dots u_m$ con $u_j = u_0 + \sum_{i = 1}^{j}{e_{l_i}}$, para $j = 1,\dots,m$, determina la $m$-trayectoria $v_0 v_1 \dots v_m$ donde $v_j = v_0 + \sum_{i = 1}^{j}{e_{\pi{(l_i)}}}$ para $j = 1,\dots,m$.
As\'i, las $m$-trayectorias que conectan $u_0$ con $u_m$ pueden ser puestas en correspondencia biyectiva con las $m$-trayectorias que conectan $v_0$ con $v_m$.

Por lo tanto, sin p\'erdida de generalidad, podemos suponer que $u_0 = 0^{(h)}0^{(n-h)}$ y  $u_m = 1^{(h)}0^{(n-h)}$.
Denotemos con $N(n,h,m)$ al n\'umero de $m$-trayectorias que conectan $u_0$ con $u_m$.
Es evidente:
\begin{itemize}
  \item Si o bien $m < h = \mbox{Hamming}(u_0,u_m)$ o $m \not \equiv h \pmod{2}$, entonces $N(n,h,m) = 0$.
  \item Si $m = h$, entonces $N(n,h,m) = m!$.
  \item Si $m > h = \mbox{Hamming}(u_0,u_m)$, $m \equiv h \pmod{2}$ y $m \geq n$, entonces $N(n,h,m) \approx m!$.
\end{itemize}

Los calculos experimentales permiten esperar que el crecimiento con $n$ de $N(n,h,m)$ supere el crecimiento de $m!$:
\[
  m \equiv h \pmod{2} \quad \implies \quad m! = o(N(n,h,m)).
\]

Para las simulaciones explicadas en la siguiente secci\'on, hemos verificado que ninguna gr\'afica de Berge aparece en las gr\'aficas de trayectorias.
Sin embargo, este resultado es suficiente para afirmar que un resultado general cumple el~\autoref{thm:non_berge_invariant}.

\begin{theorem}
  \label{thm:non_berge_invariant}
  Sea $m \in \mathbb{Z}^{+}$ una longitud fija para las trayectorias de conexi\'on.
  Si existe un $n_0 \in \mathbb{N}$ tal que para cualquier conjunto $K_0$ de $k$ parejas de v\'ertices, estos ajenos a pares, en el hipercubo $Q_{n_0}$, la gr\'afica $P_{m,k,K_0,Q_{n_0}}$ no es una gr\'afica de Berge, entonces para cualquier $n \geq n_0$ y para cualquier conjunto $K$ de $k$ parejas de v\'ertices, tambi\'en ajenos a pares, en el hipercubo $Q_n$, la gr\'afica $P_{m,k,K,Q_n}$ no es una gr\'afica de Berge.
\end{theorem}

\begin{proof}
  Sean $n_0$ y $k$ como en el~\autoref{thm:non_berge_invariant}.
  Sea $n$ un entero que cumple $n \geq n_0$ y sea $K$ una colecci\'on de $k$ parejas de v\'ertices en $Q_n$, que cumplen ser ajenas a pares.
  Sea $N_0 \subset \{1,\dots,n\}$ un subconjunto de \'indices $n_0$ y sea
  \[
    K_1 = \left\{ \left( \pi_{N_0}{(x_{0_{\kappa}})}, \pi_{N_0}{(x_{1_{\kappa}})} \right) \; | \; (x_{0_{\kappa}},x_{1_{\kappa}}) \in K \right\}
  \]
  y sea $K_0$ un subconjunto maximal de $K_1$ que consiste en parejas de v\'ertices, ajenas a pares, entonces $|K_0| = k_0 \leq k$.

  Sea $\lambda_{n_0} : Q_{n_0} \to {\{0,1\}}^{N_0}$ la identificaci\'on natural que consiste solamente de una reenumeraci\'on de las coordenadas y sea $\iota_{N_0,n} :{\{0,1\}}^{N_0} \to Q_n$ el mapa de inclusi\'on que consta del relleno con valores cero las entradas con \'indice fuera de $N_0$.

  Entonces, $\lambda_{n_0} \circ \iota_{N_0,n}$ es una inclusi\'on de $Q_{n_0}$ en $Q_n$.
  Mediante $\lambda_{n_0} \circ \iota_{N_0,n}$, la gr\'afica $P_{m,k,K_0,Q_{n_0}}$ se identifica con una subgr\'afica de $P_{m,k,K,Q_n}$.

  Dado que $P_{m,k,K_0,Q_{n_0}}$ no es una gr\'afica de Berge, entonces tampoco puede serlo $P_{m,k,K,Q_n}$.
\end{proof}

Por otro lado, se puede observar que no aparecen gr\'aficas dispersas ni sus complementos.
Evidentemente, no se producen gr\'aficas sin garras.

Por lo tanto, los m\'etodos de tiempo polinomial conocidos actualmente~\cite{grotschel1988geometric,alfonsin2001perfect} para encontrar conjuntos independientes m\'aximos no se aplican a las gr\'aficas introducidas $P_{m,k,K,Q_n}$.

%
% section problema_de_la_madeja_en_el_hipercubo (end)
%
  
\section{Algoritmos e implementaciones} % (fold)
\label{sec:algoritmos_e_implementaciones}

Para computar evidencia experimental, proponemos dos algoritmos.
El~\autoref{alg:random_path_graph_generation} para computar las gr\'afica de trayectorias y el~\autoref{alg:odd_hole_search} para la b\'usqueda de hoyos-impares en una gr\'afica de trayectorias.

\begin{algorithm}
  \DontPrintSemicolon
  \Entrada{$n$, un entero mayor o igual a $5$;\\
  $k,m$ dos enteros tales que $m \cdot k \leq |V(Q_n)|$.}
  \Salida{$P_{m,k,K,Q_n} = (V,E)$, una gr\'afica de trayectorias.}
  \BlankLine
  \Inicio{
    $h \in \mathcal{H}_{Q_n}$
    \tcp*[r]{Ciclo hamiltoniano $h$}
    $\Pi \gets paths(h,k,m)$
    \tcp*[r]{Computar $m$-trayectorias}
    $K \gets endpoints(\Pi)$
    \tcp*[r]{Computar v\'ertices extremos}
    $V \gets \emptyset$
    \tcp*[r]{V\'ertices de $P_{m,k,K,Q_n}$}
    \BlankLine
    \ParaCada{${(u,v)} \in K$}{
      Computar ${\Pi}_{u,v}$ como el conjunto de $m$-trayectorias en $Q_n$ que conectan $u$ con $v$\;
      $V \gets V \cup {\Pi}_{u,v}$
      \tcp*[r]{Agregar v\'ertices a la gr\'afica}
    }
    \BlankLine
    $E \subset V \times V$
    \tcp*[r]{Aristas de $P_{m,k,K,Q_n}$}
    \Devolver{$(V,E)$}
    \tcp*[r]{Gr\'afica $P_{m,k,K,Q_n}$ computada}
  }
  \caption{Generaci\'on aleatoria de gr\'aficas de trayectorias}
  \label{alg:random_path_graph_generation}
\end{algorithm}

\begin{algorithm}
  \DontPrintSemicolon
  \SetKwFunction{Cabeza}{cabeza}
  \SetKwFunction{Ultimo}{\'ultimo}
  \SetKwFunction{Longitud}{longitud}
  \SetKwFunction{Ciclo}{ciclo}
  \SetKwFunction{Ciclo}{ciclo}
  \Entrada{$P_{m,k,K,Q_n} = (V,E)$, una gr\'afica de trayectorias;\\
  $k$, un entero tal que $k \equiv 1 \pmod{2}$ y $k > 5$}
  \Salida{$\pi_k$, un hoyo impar si existe.}
  \BlankLine
  \Inicio{
    $v_0 \in V$
    \tcp*[r]{Tomar aleatoriamente un v\'ertice}
    $\pi \gets [v_0]$
    \tcp*[r]{Iniciar trayectoria en $v_0$}
    \BlankLine
    \Repetir{siempre}{
      \Mientras(\tcp*[f]{Llenar $\pi$}){$\Longitud(\pi) \leq k$}{
        $v_i \in N(\Ultimo(\pi))$
        \tcp*[r]{Elegir un vecino de $v_i$}
        $\pi \gets v_i$
        \tcp*[r]{Dar paso}
      }
      \SSi(\tcp*[f]{Si $\pi$ es ciclo}){$\Cabeza(\pi) = \Ultimo(\pi)$}{
        $c \gets \Ciclo(\pi)$
        \SSi{$c$}{
          \Devolver{$\pi$}
          \tcp*[r]{$k$-hoyo encontrado}
        }
      }
      Eliminar $\Cabeza(\pi)$ de $\pi$\;
    }
  }
  \caption{B\'usqueda aleatoria de un $k$-hoyo impar}
  \label{alg:odd_hole_search}
\end{algorithm}

Los programas que implementan estos algoritmos consisten en esta serie de m\'odulos b\'asicos:
\begin{enumerate}
  \item [\textbf{M\'odulo 0}]: Dado un entero positivo $n$, este m\'odulo genera un ciclo hamiltoniano $h$ en el hipercubo $Q_n$.
  \item [\textbf{M\'odulo 1}]: Dado un ciclo hamiltoniano $h$ y un entero positivo $m$, este m\'odulo parte $h$ en $k$ segmentos, generando $k$ $m$-trayectorias ajenas y sin cruces entre s\'i.
  \item [\textbf{M\'odulo 2}]: Dado una colecci\'on $\Pi$ de $m$-trayectorias ajenas y sin cruces entre si, este m\'odulo computa un conjunto $K$, con cardinalidad $k$, que contiene los extremos de las $m$-trayectorias en $\Pi$.
  \item [\textbf{M\'odulo 3}]: Dados los n\'umeros $m$ y $n$ y un conjunto $K$ de parejas de v\'ertices, este m\'odulo computa el conjunto de v\'ertices de $P_{m,k,K,Q_n}$.
  \item [\textbf{M\'odulo 4}]: Dado un conjunto de v\'ertices de una gr\'afica $P_{m,k,K,Q_n}$, este m\'odulo computa el conjunto de aristas de esa gr\'afica y el grado de cada v\'ertice; completando la gr\'afica $P_{m,k,K,Q_n}$.
  \item [\textbf{M\'odulo 5}]: Dada una gr\'afica $P_{m,k,K,Q_n}$, este m\'odulo busca hoyos impares en la gr\'afica mediante una caminata aleatoria.
\end{enumerate}

\begin{note}
  La descripci\'on de las gr\'aficas $P_{m,k,K,Q_n}$ requiere mucho espacio y, por lo tanto, es importante dise\~nar estructuras de datos enfocados a minimizar este espacio requerido y algoritmos y que permiten realizar eficientemente operaciones en estas gr\'aficas.
  Nuestros programas fueron escritos en el lenguaje Ruby con el objetivo de obtener, a la brevedad posible, evidencia experimental.
  El c\'odigo de estos est\'a libre y est\'a disponible~\cite{israel2014mpath}.
\end{note}

%
% section algoritmos_e_implementaciones (end)
%

\section{Contribuciones} % (fold)
\label{sec:contribuciones}

El trabajo que presentamos en este cap\'itulo introduce una familia de problemas conocidos como \emph{``problemas de madeja''}.
Los presentamos aplicados tanto a gr\'aficas en general y como en el hipercubo.
Estos problemas se reducen al problema de encontrar conjuntos independientes en las gr\'aficas.
Esto es en s\'i mismo es un problema NP-dif\'icil e incluso es PTAS-dif\'icil, es decir, dif\'icil de aproximar.
No obstante, presentamos evidencia de que las gr\'aficas reducidas obtenidas no son gr\'aficas perfectas, por lo que no son adecuadas para tratarse por los enfoques de tiempo polinomial actualmente conocidos para resolver el problema de independencia.

Observemos que, a partir de un ciclo hamiltoniano en una gr\'afica, es f\'acil mostrar una familia de trayectorias ajenas y sin cruces por parejas que conectan, obviamente, las parejas de v\'ertices extremos.
Sin embargo, lo contrario de este problema es extremadamente dif\'icil.
Este comportamiento donde un proceso es sencillo se resolver pero el proceso inverno es muy complicado, es ampliamente aprovechado en areas como la criptograf\'ia, por lo tanto, esto podr\'ia usarse en protocolos criptogr\'aficos donde la clave p\'ublica corresponde a los v\'ertices del extremo de la trayectoria, y la clave privada es solo la madeja, es decir, el conjunto de trayectorias sin cruces que las conectan de forma pareja.

%
% section contribuciones (end)
%

\section{Conclusiones} % (fold)
\label{sec:conclusiones}

La b\'usqueda de conjuntos independientes m\'aximos en gr\'aficas es un problema NP-dif\'icil cl\'asico.
Esto hace posible que sea aprovechado en protocolos de autenticaci\'on.
Dada una gr\'afica enorme y un conjunto independiente m\'aximo en esta gr\'afica, estas pueden ser la clave privada y la clave p\'ublica, respectivamente, de un protocolo de autenticaci\'on en una versi\'on ingenua o no optimizada.

Esto debido a que surgen algunas dificultades:
\begin{inparaenum}[1.]
  \item la descripci\'on de la gr\'afica puede requerir una gran cantidad de datos lo que es poco pr\'actico para ofrecer una seguridad aceptable, y
  \item la b\'usqueda de conjuntos independientes m\'aximos, lo que ser\'ia la clave p\'ublica en un protocolo, tambi\'en puede ser poco pr\'actico debido a su costo computacional.
\end{inparaenum}

Los problemas de la madeja anulan estas dificultades, particularmente en las gr\'aficas de trayectorias en el hipercubo.
Estas gr\'aficas desempe\~nan el papel de instancias dif\'iciles y los conjuntos independientes m\'aximos correspondientes que se utilizar\'an como claves p\'ublicas se pueden encontrar f\'acilmente en los ciclos hamiltonianos en el hipercubo.
Por otro lado, encontrar conjuntos independientes m\'aximos en gr\'aficos de ruta es un problema muy dif\'icil debido a su tama\~no, incluso para hipercubos de dimensiones relativamente peque\~nas.

En este trabajo~\cite{feliu2016trajectory} introdujimos los problemas de madeja y discutimos sus propiedades matem\'aticas m\'as b\'asicas en un enfoque puramente de teor\'ia de gr\'aficas.

%
% section conclusiones (end)
%

%
% chapter grafica_de_trayectorias_y_problemas_de_la_madeja (end)
%

%!TEX root = ../../predoc-plain.tex

\tikzstyle{vertex} = [%
  circle,%
  fill=MaterialBlue100,%
  draw=MaterialBlue700,%
  text=MaterialBlue800,%
  minimum size=25px,%
  scale=1];

\tikzstyle{endpoint} = [%
  circle,%
  fill=MaterialPurple100,%
  draw=MaterialPurple700,%
  text=MaterialPurple800,%
  minimum size=25px,%
  line width=1px];

\tikzstyle{solution_edge} = [%
  draw=MaterialDeepPurple700,%
  line width=1px];

\tikzstyle{solution_edge_a} = [%
  draw=MaterialDeepPurple700,%
  dashed,%
  line width=1px];

\tikzstyle{edge} = [%
  draw=MaterialBlue700,%
  loosely dotted,%
  line width=1px];

% Vertices
\node  (v0) at (0.0,0.0) [endpoint]   {$v_0$};
\node  (v1) at (1.5,0.0) [vertex]   {$v_1$};
\node  (v2) at (3.0,0.0) [vertex]   {$v_2$};
\node  (v3) at (4.5,0.0) [vertex]   {$v_3$};
\node  (v4) at (4.5,1.5) [endpoint]   {$v_4$};
\node  (v5) at (3.0,1.5) [vertex]   {$v_5$};
\node  (v6) at (1.5,1.5) [endpoint]   {$v_6$};
\node  (v7) at (0.0,1.5) [endpoint]   {$v_7$};
\node  (v8) at (0.0,3.0) [endpoint]   {$v_8$};
\node  (v9) at (1.5,3.0) [vertex]   {$v_9$};
\node  (v10) at (3.0,3.0) [vertex]   {$v_{10}$};
\node  (v11) at (4.5,3.0) [vertex]   {$v_{11}$};
\node  (v12) at (4.5,4.5) [endpoint]   {$v_{12}$};
\node  (v13) at (3.0,4.5) [vertex]   {$v_{13}$};
\node  (v14) at (1.5,4.5) [vertex]   {$v_{14}$};
\node  (v15) at (0.0,4.5) [vertex]   {$v_{15}$};

% Edges

\draw [-,solution_edge] (v0) to (v1) to (v2) to (v3) to (v4);
\draw [-,solution_edge] (v12) to (v13) to (v14) to (v15) to (v8);
\draw [-,solution_edge] (v7) to (v9) to (v10) to (v5) to (v6);

\draw [-,solution_edge_a] (v0) to (v6);
\draw [-,solution_edge_a] (v4) to (v11);
\draw [-,solution_edge_a] (v11) to (v12);
\draw [-,solution_edge_a] (v7) to (v8);

\draw [-,edge] (v6) to (v8);
\draw [-,edge] (v2) to (v5);
\draw [-,edge] (v4) to (v5);
\draw [-,edge] (v10) to (v11);
\draw [-,edge] (v10) to (v13);
\draw [-,edge] (v8) to (v13);
\draw [-,edge] (v2) to (v7);
\draw [-,edge] (v2) to (v9);
\draw [-,edge] (v6) to (v13);
\draw [-,edge] (v2) to (v12);
\draw [-,edge] (v3) to (v13);
